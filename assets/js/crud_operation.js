$(document).ready(function () {
	listClient();
	$('#listClientTable').dataTable({
		"bPaginate": false,
		"bInfo": false,
		"bFilter": false,
		"bLengthChange": false,
		"pageLength": 5
	});
	// list all user in datatable
	function listClient() {
		$.ajax({
			type: 'ajax',
			url: 'client/tampilkanData',
			async: false,
			dataType: 'json',
			success: function (data) {
				var html = '';
				var i;
				var no = 1;
				for (i = 0; i < data.length; i++) {
					html += '<tr id="' + data[i].id_client + '">' +
						'<td>' + no++ + '</td>' +
						'<td>' + data[i].nama_client + '</td>' +
						'<td>' + data[i].alamat_client + '</td>' +
						'<td>' + data[i].no_hp + '</td>' +
						'<td style="text-align:right;">' +
						'<a href="javascript:void(0);" class="btn btn-info btn-sm editRecord" data-id="' + data[i].id_client + '" data-nama="' + data[i].nama_client + '"data-alamat="' + data[i].alamat_client + '"data-phone="' + data[i].no_hp + '">Edit</a>' + ' ' +
						'<a href="javascript:void(0);" class="btn btn-danger btn-sm deleteRecord" data-id="' + data[i].id_client + '">Delete</a>' +
						'</td>' +
						'</tr>';
				}
				$('#listClient').html(html);
			}

		});
	}

	// save new user record
	$('#saveClientForm').submit('click', function () {
		var nama = $('#nama').val();
		var alamat = $('#alamat').val();
		var phone = $('#phone').val();
		$.ajax({
			type: "POST",
			url: "client/simpanData",
			dataType: "JSON",
			data: { nama: nama, alamat: alamat, phone: phone },
			success: function (data) {
				$('#nama').val("");
				$('#alamat').val("");
				$('#phone').val("");
				$('#addClientModal').modal('hide');
				listClient();
			}
		});
		return false;
	});

	// show edit modal form with user data
	$('#listClient').on('click', '.editRecord', function () {
		$('#editClientModal').modal('show');
		$("#clientId").val($(this).data('id'));
		$("#namaEdit").val($(this).data('nama'));
		$("#alamatEdit").val($(this).data('alamat'));
		$("#phoneEdit").val($(this).data('phone'));
	});

	// save edit record
	$('#editClientForm').on('submit', function () {
		var clientId = $('#clientId').val();
		var namaEdit = $('#namaEdit').val();
		var alamatEdit = $('#alamatEdit').val();
		var phoneEdit = $('#phoneEdit').val();
		$.ajax({
			type: "POST",
			url: "client/update",
			dataType: "JSON",
			data: { clientId: clientId, namaEdit: namaEdit, alamatEdit: alamatEdit, phoneEdit: phoneEdit },
			success: function (data) {
				$("#clientId").val("");
				$("#namaEdit").val("");
				$('#alamatEdit').val("");
				$('#phoneEdit').val("");
				$('#editClientModal').modal('hide');
				listClient();
			}
		});
		return false;
	});

	// show delete modal
	$('#listClient').on('click', '.deleteRecord', function () {
		var ClientId = $(this).data('id');
		$('#deleteClientModal').modal('show');
		$('#deleteClientId').val(ClientId);
	});
	
	// delete user record
	$('#deleteClientForm').on('submit', function () {
		var ClientId = $('#deleteClientId').val();
		$.ajax({
			type: "POST",
			url: "client/hapus",
			dataType: "JSON",
			data: { clientId: ClientId },
			success: function (data) {
				$("#" + ClientId).remove();
				$('#deleteClientId').val("");
				$('#deleteClientModal').modal('hide');
				listClient();
			}
		});
		return false;
	});
});
