<?php

class Client_model extends CI_Model
{
    public function getAllUser()
    {
        return $this->db->get('client')->result();
    }


    public function inputData()
    {
        $data = [
            'nama_client' => $this->input->post('nama', true), //mengamankan data dengan parameter true, pake XSS filter seperti binding
            'alamat_client' => $this->input->post('alamat', true),
            'no_hp' => $this->input->post('phone', true)
        ];

        return $this->db->insert('client', $data);
    }

    function updateData()
    {
        $clientid = $this->input->post('clientId'); //diambil dari data-clientId
        $namaEdit = $this->input->post('namaEdit', true);
        $alamatEdit = $this->input->post('alamatEdit', true);
        $phone = $this->input->post('phoneEdit', true);

        $this->db->set('nama_client', $namaEdit);
        $this->db->set('alamat_client', $alamatEdit);
        $this->db->where('id_client', $clientid);
        $this->db->set('no_hp', $phone);
        return $this->db->update('client');
    }

    public function hapusData()
    {
        $clientid = $this->input->post('clientId');
        $this->db->where('id_client', $clientid);
        return $this->db->delete('client');
    }
}
