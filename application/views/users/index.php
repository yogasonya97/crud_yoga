<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>User Management</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/jquery.dataTables.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/dataTables.bootstrap4.css' ?>">

</head>

<body>
	<div class="container mt-5">
		<a href="<?php base_url();?>login/logout" class="btn btn-primary">Logout</a>
		<h2>Data Client</h2>
		<p>Anda dapat mengelola data client</p>
		<table class="table table-striped border" id="listClientTable">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Client</th>
					<th>Alamat Client</th>
					<th>No.Hp</th>
					<th style="text-align: right;">Action</th>
				</tr>
			</thead>
			<tbody id="listClient">
				<!-- Untuk menampilkan datanya, menggunakan JQuery + AJAX -->
			</tbody>
		</table>
		<div><a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#addClientModal"><span class="fa fa-plus"></span> Tambah Data</a></div><br>
	</div>

	<!-- Modal Tambah Data Client -->
	<form id="saveClientForm" method="post">
		<div class="modal fade" id="addClientModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Tambah Data Client</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Nama*</label>
							<div class="col-md-10">
								<input type="text" name="nama" id="nama" class="form-control" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Alamat*</label>
							<div class="col-md-10">
								<input type="text" name="alamat" id="alamat" class="form-control" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">No.HP*</label>
							<div class="col-md-10">
								<input type="text" name="phone" id="phone" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</div>
	</form>

	<form id="editClientForm" method="post">
		<div class="modal fade" id="editClientModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="editModalLabel">Edit Data Client</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Nama Client*</label>
							<div class="col-md-10">
								<input type="text" name="namaEdit" id="namaEdit" class="form-control" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Alamat Client*</label>
							<div class="col-md-10">
								<input type="text" name="alamatEdit" id="alamatEdit" class="form-control" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Phone*</label>
							<div class="col-md-10">
								<input type="text" name="phoneEdit" id="phoneEdit" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="clientId" id="clientId" class="form-control">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
						<button type="submit" class="btn btn-primary">Yes</button>
					</div>
				</div>
			</div>
		</div>
	</form>

	<form id="deleteClientForm" method="post">
		<div class="modal fade" id="deleteClientModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deleteModalLabel">Delete User</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Are you sure to delete this record?</p>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="deleteClientId" id="deleteClientId" class="form-control">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
						<button type="submit" class="btn btn-primary">Yes</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery-3.2.1.js' ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery.dataTables.js' ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/dataTables.bootstrap4.js' ?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/crud_operation.js' ?>"></script>

</body>

</html>