<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function login_process()
    {
       
        $username = $this->input->post('username'); 
        $password = md5($this->input->post('password')); 

        $result = $this->User_model->check_user($username, $password);

        if($result->num_rows() > 0){ 
                foreach ($result->result() as $row) { 
                    $id_user = $row->id_user;
                    $username = $row->username; 
                    $email  = $row->email;
                    $level_user = $row->level_user;
                }

            $newdata = array( 
            'id_user' => $id_user, 
            'username' => $username,
            'email' => $email,
            'level_user' => $level_user, 
            'logged_in' => TRUE 
            ); 
            //set up session data 
            $this->session->set_userdata($newdata); 
            if($this->session->userdata('level_user') == '1'){
                redirect('client'); 
                }
        }else { 
                        ?> 
                        <script type="text/javascript">alert("Maaf username atau password anda salah."); 
                        window.location.href="<?php echo base_url();?>"</script> <?php 
                        }
    
    }

    public function logout(){
            $this->session->sess_destroy();
            $url=base_url();
            redirect($url);
        }


    // function tampilkanData()
    // {
    //     $data = $this->User_model->getAllUser();
    //     echo json_encode($data);
    // }

    // public function simpanData()
    // {
    //     $data = $this->User_model->inputData();
    //     echo json_encode($data);
    // }

    // function update()
    // {
    //     $data = $this->User_model->updateData();
    //     echo json_encode($data);
    // }
    // public function hapus()
    // {
    //     $data = $this->User_model->hapusData();
    //     echo json_encode($data);
    // }
}
